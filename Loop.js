/*
Soal No. 1 Looping While
Anda diminta untuk membuat looping dengan while supaya output yang dihasilkan :

LOOPING PERTAMA
2 - I love coding
4 - I love coding
6 - I love coding
8 - I love coding
10 - I love coding
12 - I love coding
14 - I love coding
16 - I love coding
18 - I love coding
20 - I love coding

*/

let x = 2;

while (x <= 20) {
    console.log(x, "- I love Coding");
    x = x + 1;
    x++;
}


// ============================================================================

/*
Soal No.2 Looping menggunakan for
Buatlah looping dengan syarat: 
  - Jika angkanya ganjil maka tampilkan "Semangat"
  - Jika angkanya genap maka tampilkan "Berkualitas"
  - Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan "I Love Coding"
*/
console.log('\n')

for (let i = 1; i <= 10; i++) {    
  if (i % 2 == 0) {
      console.log('Berkualitas')
  } else if (i % 3 == 0){
      console.log('I love Coding')
  } else {
      console.log('Semangat')
  }
}