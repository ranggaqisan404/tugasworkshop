/*
1. Buatlah sebuah fungsi bernama minimal dengan ketentuan berikut: 
  - Menerima dua buah argumen dengan tipe data number, yang bernama a dan b.
  - Return nilai terkecil antara a atau b.
  - Bila nilai keduanya sama, maka return dengan nilai a.

  contoh:
    minimal(1,4) //Output: 1
    minimal(3,2) //Output: 2
    minimal(3,3) //Output: 3

2. Buatlah sebuah fungsi bernama kalikan dengan ketentuan berikut:
  - Menerima dua buah argumen dengan tipe data number, yang bernama a dan b.
  - Return hasil dari perkalian antara a dan b.
*/

console.log('\n');
function minimal (a,b) {
  if ( a > b) {
    return b;
  } else if ( a < b ) {
    return a;
  } else {
    return a;
  }
}
console.log('Hasil minimal a dengan b : ' + minimal(6,3));

function kalikan (a,b) {
  return a * b ;
}
console.log('Hasil perkalian a dengan b : ' + kalikan(5,4));
console.log('\n');

//Hiraukan kode di bawah ini
module.exports = {kalikan, minimal}