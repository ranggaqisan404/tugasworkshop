/*
1. Buatlah logika if untuk mengevaluasi skor yang diberikan dengan ketentuan: 
  a. Jika nilai skor lebih atau sama dengan 90
    - Isi variabel result dengan nilai: "Selamat! Anda mendapatkan nilai A."
  b. Jika nilai skor ada di antara 80 hingga 89
    - Isi variabel result dengan nilai: "Anda mendapatkan nilai B."
  c. Jika nilai skor ada di antara 70 hingga 79
    - Isi variabel result dengan nilai: "Anda mendapatkan nilai C."
  d. Jika nilai skor ada di antara 60 hingga 69
    - Isi variabel result dengan nilai: "Anda mendapatkan nilai D."
  e. Jika nilai skor ada di bawah 60
    - Isi variabel result dengan nilai: "Anda mendapatkan nilai E."
*/

function cekScore(score) {
  let result;

  if (score >= 90) {
      console.log('Selamat! Anda mendapatkan nilai A.');
  } else if (score >= 80 && score <= 89) {
      console.log('Anda mendapatkan nilai B.');
  } else if (score >= 70 && score <= 79) {
      console.log('Anda mendapatkan nilai C.');
  } else if (score >= 60 && score <= 69) {
      console.log('Anda mendapatkan nilai D.')
  } else {
      console.log('Anda mendapatkan nilai E.')
  }

  return result;
};

console.log(cekScore(20));

//Abaikan kode di bawah ini.
module.exports = {cekScore};